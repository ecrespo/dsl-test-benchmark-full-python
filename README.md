DSL test benchmark
------------------

Encuentralo en: https://bitbucket.org/phenobarbital/dsl-test-benchmark
Copyright: 2013 Jesus Lara <jesuslara@gmail.com>
Licencia: GPL V.3

Instalacion
-----------

Para instalarlo, solo deben tener:

* python-argparse
* python-lxml
* curl
* wget
* dig (parte de los DNS utils)

Uso
---

Simplemente se ejecuta el script:

./testdsl.sh

Esto emitiráel sumario con los valores.
